#Ugly numbers calculator

def uglyNum(n):
    '''Fucntion to calculate Nth ugly number'''
    #inittialising Factors of 2,3,5
    mul2 = mul3 = mul5 = 1
    factors_of_2 = 2*mul2
    factors_of_3 = 3*mul3
    factors_of_5 = 5*mul5

    for i in range(1,n):
        num = min(factors_of_2,factors_of_3,factors_of_5)
        if num == factors_of_2:
            mul2 += 1
            factors_of_2 = 2*mul2
        elif num == factors_of_3:
            mul3 += 1
            factors_of_3 = 3 * mul3 
        else:
            mul5 += 1
            factors_of_5 = 5 * mul5   
        if i == n:
            return num

if __name__ == '__main__':
    try:
        n = int(input("Enter Nth number \n-->"))
    except ValueError:
        print("Invalid Input")
    if n == 0:
        print("Zero is not a valid index")
    else:
        print(uglyNum(n))