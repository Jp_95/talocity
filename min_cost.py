''' Module to compute the mininmum cost to carry a weight given a list of bags with different
    capacities and costs
Method defined :
    minCost(list of Bag, Weight to be carried)
        where Bag is a named tuple `Bag` with `capacity` and `cost` as attributes
        if Bag is not used as specified the results would be undefined
'''
from collections import namedtuple

Bag = namedtuple('Bag', 'capacity cost')

def minCost(bags, weight):
    '''Function to find the bags from the list with minimum cost to carry the specified weight'''
    print(str(type(bags[0])))
    if str(type(bags[0])) !="<class '" +__name__+".Bag'>":
        return "Invalid Input Use Bag object"
    #cleaning bags to accomodate Bag of capacity < required weight for optimisation
    minima = 99999
    cap = []
    for i, bag in enumerate(bags[:]):
        if bag.capacity > weight:
            if bag.cost < minima:
                minima = bag.cost
                cap.append(bag)
            bags.remove(bag)
    cumulative_capacity = 0
    for i, bag in enumerate(bags[:]):
        cumulative_capacity += bag.capacity
    if cumulative_capacity < weight:
        if minima == 99999:
            return 0, "Not Applicable1"
        return minima, cap[-1]

    #inititalising solution matrix
    soltn_matrix = [[0 for x in range(weight+1)] for x in range(len(bags)+1)]

    #initialising 1st row
    for i in range(weight+1):
        soltn_matrix[0][i] = 99999

    #initialising 1st column
    for i in range(1, len(bags)+1):
        soltn_matrix[i][0] = 0

    #Creating the solution matrix
    for i in range(1, len(bags)+1):
        for j in range(1, weight+1):
            if bags[i-1].capacity > j:
                soltn_matrix[i][j] = soltn_matrix[i-1][j]
            else:
                soltn_matrix[i][j] = min(soltn_matrix[i-1][j],
                                         soltn_matrix[i][j-bags[i-1].capacity] + bags[i-1].cost)
    #Logic for selecting the bags based on solution matrix
    #matrix to hold the Selected bags
    result = []
    #initialisation
    i = len(bags)
    j = weight
    while i > 0 and j > 0:
        if soltn_matrix[i][j] != soltn_matrix[i-1][j]:
            result.append(bags[i-1])
            j = j - bags[i-1].capacity
            i = i-1
        else:
            i = i-1
    # print("soltnMatrix", soltn_matrix)
    # print(result,"WEIGHT",weight)
    if soltn_matrix[len(bags)][weight] == 99999:
        return "Not Applicable"
    else:
        if soltn_matrix[len(bags)][weight] > minima and  minima != 99999:
            return minima,cap[-1]
        return soltn_matrix[len(bags)][weight], result
    # return "Minimum cost : ", "Not possible to accomodate weight"
    # if soltn_matrix[len(bags)][weight] == 99999
    # else soltn_matrix[len(bags)][weight],"Bags used : ",result


if __name__ == '__main__':

    def main():
        'Driver function'
        while True:
            try:
                weight = int(input("Enter the Weight that is to be stored : "))
                num_bags = int(input("Enter the number of bags available : "))
                break
            except ValueError:
                print("Incorrect values. Use only +ve Numbers")    
        capacities = [] #List to store the capacities of different bags
        costs = [] #list to store the costs of different bags
        for i in range(1, num_bags+1):
            while True:
                try:
                    capacities.append(int(input("Enter weight capacity of Bag %s --> "%(i))))
                    costs.append(int(input("Enter cost  of Bag %s           --> "%(i))))
                    break
                except ValueError:
                    print("Incorrect Input. Try using +ve Numbers only")
        # cost_capacity = list(zip(costs,capacities))
        # cost_capacity = sorted(cost_capacity, key=lambda a:a[-1])
        bags = [Bag(cost=costs[x], capacity=capacities[x])   for x in range(len(costs))]
        print("Available Bags", bags)
        print("Weight to Accomodate", weight)
        print("Result\n(MinimumCost, BagsUsed) : ", minCost(bags, weight))

    main()
